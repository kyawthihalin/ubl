@php
    $time1 = strtotime($start_date);
    $time2 = strtotime($end_date);
    $start_date = date('Y-m-d',$time1);
    $end_date = date('Y-m-d',$time2);
    $count = count($customers);

@endphp
<div id="DivIdToPrint">
    
    
    @if($count != 0)  
    <div>
        <table class="table-data" style="border:white;">
            <tr style="border:white">
                <td style="border:white"></td>
                <td style="text-align: center;border:white"> <img src="{{asset('logo/logo.jpg')}}" alt="" style="width: 200px;height:100px">  </td>
                <td style="border:white"></td>
            </tr>
            <tr style="border:white">
                <td style="border:white;width:30%"></td>
                <td style="text-align: center;border:white;width:35%;"> <h5 style="font-weight: bold;">Customer Report</h5> </td>
                <td style="border:white;width:35%;text-align:right"> <small>Data From {{$start_date}} to {{$end_date}}</small> </td>
            </tr>
        </table>
    </div>
    <table class="table-data" id="table-data">
            <thead>
            <tr>
                <th>Customer ID</th>
                <th>Customer Name</th>
                <th>Ward</th>
                <th>Home No</th>
                <th>Amount</th>
                <th>Currency Unit</th>
                <th>Created Date</th>
                <th>Updated Date</th>
            </tr>
            </thead>
            <tbody>
                @foreach($customers as $customer)
                @php
                $created = strtotime($customer->created_at);
                $created = date('Y-m-d',$created);
                $Updated = strtotime($customer->updated_at);
                $Updated = date('Y-m-d',$Updated);
                $unit    = \App\Models\Currency::find($customer->currency_id);
                @endphp
                <tr>
                    <td>{{$customer->customer_id}}</td>
                    <td>{{$customer->customer_name}}</td>
                    <td>{{$customer->ward->ward_name}}</td>
                    <td>{{$customer->home_no}}</td>
                    <td>{{$customer->amount}}</td>
                    <td>{{$unit->name}}</td>
                    <td>{{$created}}</td>
                    <td>{{$Updated}}</td>
                </tr>
                @endforeach
            </tbody>
    </table>
    @else
        <div class="row mt-5">
            <div class="offset-md-5">
               <img src="{{asset('logo/sorry.gif')}}" alt="" style="width: 200px;height:200px"> 
            </div>
        </div>
        <h5 class="text-center mt-5" style="font-weight: bold;">Opps! There is no data From ({{$start_date}}) To ({{$end_date}}).</h5>
    @endif
</div>
