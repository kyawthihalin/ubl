@php
$wards = \App\Models\Ward::all();
if($entry){
    $Ecustomer = \App\Models\Customer::find($entry->customer_id);
    $Eward = \App\Models\Ward::find($Ecustomer->ward_id);
    $Ecustomers = \App\Models\Customer::where('ward_id',$Eward->id)->get();
}
@endphp
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<div class="form-group">
    <label for="ward">Choose Ward</label>
    <select class="form-control s2" id="ward" name="ward">
        <option>Please Choose Ward</option>
        @foreach($wards as $ward)
        @if(isset($Eward))
        @if($Eward->id == $ward->id)
        <option value="{{$ward->id}}" selected>{{$ward->ward_name}}</option>
        @else
        <option value="{{$ward->id}}">{{$ward->ward_name}}</option>
        @endif
        @else
        <option value="{{$ward->id}}">{{$ward->ward_name}}</option>
        @endif
        @endforeach
    </select>
</div>
<div>
    <label for="customer">Choose Customer</label>
    @if ($errors->has('customer_id'))
    <select class="form-control is-invalid" id="customer" name="customer_id">
    @else
    <select class="form-control" id="customer" name="customer_id">
    @endif
        @if(isset($Ecustomer))
        @foreach($Ecustomers as $cus)
        @if($cus->id == $Ecustomer->id)
        <option value="{{$cus->id}}" selected>{{$cus->customer_name}}</option>
        @else
        <option value="{{$cus->id}}">{{$cus->customer_name}}</option>
        @endif
        @endforeach
        @else
        <option value="">Please Choose Customer</option>
        @endif
    </select>
</div>
@push('after_scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $(document).ready(function(){
            $('.s2').select2({});
        })
        $('#ward').on('change', function() {
            let ward_id = $('#ward').val();
            $.ajax({
                url: '{{route('getcustomers')}}',
                type: 'POST',
                async: false,
                data: {
                    ward_id
                },
                success: function(responese) {
                    $('#customer').html(responese);
                },
            });

        })
    });
</script>
@endpush
