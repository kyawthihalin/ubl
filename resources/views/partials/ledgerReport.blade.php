@php
    $time1 = strtotime($start_date);
    $time2 = strtotime($end_date);
    $start_date = date('Y-m-d',$time1);
    $end_date = date('Y-m-d',$time2);
    $count = count($ledgers);
@endphp
<div id="DivIdToPrint">
   
    
    @if($count != 0 )  
    <div>
        <table class="table-data" style="border:white;" >
            <tr style="border:white">
                <td style="border:white"></td>
                <td style="text-align: center;border:white"> <img src="{{asset('logo/logo.jpg')}}" alt="" style="width: 200px;height:100px">  </td>
                <td style="border:white"></td>
            </tr>
            <tr style="border:white">
                <td style="border:white;width:30%"></td>
                <td style="text-align: center;border:white;width:35%;"> <h5 style="font-weight: bold;">Ledger Report</h5> </td>
                <td style="border:white;width:35%;text-align:right"> <small>Data From {{$start_date}} to {{$end_date}}</small> </td>
            </tr>
        </table>
    </div>
    <table class="table-data" id="table-data">
            <thead>
            <tr>
                <th>Ledger ID</th>
                <th>Customer ID</th>
                <th>Customer Name</th>
                <th>Ward</th>
                <th>Currency</th>
                <th>Print Status</th>
                <th>Printed Date</th>
                <th>Amount</th>
                <th>Pay Back Amount</th>
                <th>Reason</th>
                <th>Total Amount</th>

            </tr>
            </thead>
            <tbody>
                @foreach($ledgers as $ledger)
                @php
                $ledgerDate = strtotime($ledger->ledger_date);
                $ledgerDate = date('Y-m-d',$ledgerDate);
                $printedDate = strtotime($ledger->updated_at);
                $printedDate = date('Y-m-d',$printedDate);
                $customer_id = $ledger->customer_id;
                $customer = \App\Models\Customer::find($customer_id);
                @endphp
                <tr>
                    <td>{{$ledger->ledger_id}}</td>
                    <td>{{$ledger->customer->customer_id}}</td>
                    <td>{{$ledger->customer->customer_name}}</td>
                    <td>{{$customer->ward->ward_name}}</td>
                    <td>{{$ledger->currency->name}}</td>
                    @if($ledger->print_status == 1)
                    <td>Printed</td>
                    <td>{{$ledgerDate}}</td>
                    @else
                    <td> Not Printed </td>
                    <td>-</td>
                    @endif
                    <td>{{$ledger->amount}} ({{$ledger->currency->name}})</td>
                    <td>{{$ledger->pay_back}}({{$ledger->currency->name}})</td>
                    <td>{{$ledger->reason}}</td>
                    <td>{{$ledger->total}}({{$ledger->currency->name}})</td>

                </tr>
                @endforeach
                <tr>
                    <td colspan="10" style="text-align:right;font-weight:bold">Total Amount-</td>
                    <td>{{$ledgerTotal}} (MMK) & {{$ledgerTotalBaht}} (Baht)</td>
                </tr>
            </tbody>
    </table>
    @else
        <div class="row mt-5">
            <div class="offset-md-5">
               <img src="{{asset('logo/sorry.gif')}}" alt="" style="width: 200px;height:200px"> 
            </div>
        </div>
        <h5 class="text-center mt-5" style="font-weight: bold;">Opps! There is no data From ({{$start_date}}) To ({{$end_date}}).</h5>
    @endif
</div>
