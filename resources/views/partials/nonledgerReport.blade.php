@php
    $time1 = strtotime($startDate);
    $time2 = strtotime($endDate);
    $start_date = date('Y-m-d',$time1);
    $end_date = date('Y-m-d',$time2);
    $count = count($non_printed_customers);
@endphp
<div id="DivIdToPrint">
   
    
    @if($count != 0 )  
    <div>
        <table class="table-data" style="border:white;" >
            <tr style="border:white">
                <td style="border:white"></td>
                <td style="text-align: center;border:white"> <img src="{{asset('logo/logo.jpg')}}" alt="" style="width: 200px;height:100px">  </td>
                <td style="border:white"></td>
            </tr>
            <tr style="border:white">
                <td style="border:white;width:30%"></td>
                <td style="text-align: center;border:white;width:35%;"> <h5 style="font-weight: bold;">Non Printed Ledger Report</h5> </td>
                <td style="border:white;width:35%;text-align:right"> <small>Data From {{$start_date}} to {{$end_date}}</small> </td>
            </tr>
        </table>
    </div>
    <table class="table-data" id="table-data">
            <thead>
            <tr>
                <th>Customer ID</th>
                <th>Customer Name</th>
                <th>Ward</th>
                <th>Currency</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
                @foreach($non_printed_customers as $ledger)
                <tr>
                    <td>{{$ledger->customer_id}}</td>
                    <td>{{$ledger->customer_name}}</td>
                    <td>{{$ledger->ward->ward_name}}</td>
                    <td>{{$ledger->currency->name}}</td>
                    <td>{{$ledger->amount}}({{$ledger->currency->name}})</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="4" style="text-align:right;font-weight:bold">Total Amount-</td>
                    <td>{{$totalMMK}} (MMK) & {{$totalBaht}} (Baht)</td>
                </tr>
            </tbody>
    </table>
    @else
        <div class="row mt-5">
            <div class="offset-md-5">
               <img src="{{asset('logo/sorry.gif')}}" alt="" style="width: 200px;height:200px"> 
            </div>
        </div>
        <h5 class="text-center mt-5" style="font-weight: bold;">Opps! There is no data From ({{$start_date}}) To ({{$end_date}}).</h5>
    @endif
</div>
