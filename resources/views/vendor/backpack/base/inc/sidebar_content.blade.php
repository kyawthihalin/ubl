<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
@if(backpack_user()->can('dashboard'))
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon text-white"></i> <span class="text-white">Dashboard</span></a></li>
@endif
@if(backpack_user()->can('list-ledger'))
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('ledger') }}'><i class='nav-icon la la-book'></i> <span class="text-white"> Ledgers</span> </a></li>
@endif
@if(backpack_user()->can('list-ward'))
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('ward') }}'><i class="nav-icon la la-bank text-white"></i> <span class="text-white">Wards</span></a></li>
@endif
{{--@if(backpack_user()->can('list-currency'))
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('currency') }}'><i class='nav-icon la la-money'></i><span class="text-white"> Currencies </span></a></li>
@endif--}}
@if(backpack_user()->can('list-customer'))
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('customer') }}'><i class='nav-icon la la-user'></i><span class="text-white">  Customers</span> </a></li>
@endif
@if(backpack_user()->can('reporting'))
<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users text-white"></i> <span class="text-white">Reporting</span> </a>
	<ul class="nav-dropdown-items">
		@if(backpack_user()->can('ledger-report'))
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('reporttransaction/create') }}'><i class='nav-icon la la-file-text-o'></i><span class="text-white"> Ledger Reports</span> </a></li>
		@endif
		@if(backpack_user()->can('ward-report'))
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('wardreporttransaction/create') }}'><i class='nav-icon la la-file-text-o'></i><span class="text-white"> Ward Reports</span></a></li>
		@endif
		@if(backpack_user()->can('customer-report'))
		<li class='nav-item'><a class='nav-link' href='{{ backpack_url('customerreporttransaction/create') }}'><i class='nav-icon la la-file-text-o'></i><span class="text-white">Customer Reports</span></a></li>
		@endif
	</ul>
</li>
@endif
@if(backpack_user()->hasRole('Super Admin'))
<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users text-white"></i> <span class="text-white">Authentication</span> </a>
	<ul class="nav-dropdown-items">
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-group text-white"></i> <span class="text-white">Users & Teams</span></a></li>
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge text-white"></i> <span class="text-white">Roles</span></a></li>
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key text-white"></i> <span class="text-white">Permissions</span></a></li>
	</ul>
</li>
@endif
