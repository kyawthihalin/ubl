@extends(backpack_view('blank'))
@section('content')
<script type="text/javascript">
  function display_c() {
    var refresh = 1000; // Refresh rate in milli seconds
    mytime = setTimeout('display_ct()', refresh)
  }

  function display_ct() {
    var x = new Date();
    var h = x.getHours(),
      m = x.getMinutes() + ":" + x.getSeconds();
    var ime = (h > 12) ? (h - 12 + ':' + m + ' PM') : (h + ':' + m + ' AM');
    document.getElementById('ct').innerHTML = ime;
    display_c();
  }
</script>
<section class="content-header" style="padding-top: 5px">
  <strong>
    <h1 class="container-fluid" style="font-family:Times New Roman; font-size:35px; font-weight: bold !important; color: #404e67;">
      Welcome Back, {{Auth::user()->name}} !
    </h1>
  </strong>
  <h3 class="container-fluid mb-5" style="padding-top: 0px;margin-top: 10px;">
    <small style="font-family: Centaur; font-size: 20px; font-weight: bold; color: #60099e;">
      Today is <?php
                $date = Carbon\Carbon::now();
                echo date('l, d F Y', strtotime($date)); //June, 2017
                ?>

      <body onload=display_ct();>
        <span id='ct'></span>
      </body>
    </small>
  </h3>
</section>
@endsection
<?php
$widgets['after_content'][] =
  [
    'type' => 'div',
    'class' => 'container-fluid row',
    'content' => [ // widgets 
      [
        'type'          => 'progress_white',
        'class'         => 'card mb-5',
        'value'         => $mmk . ' MMK & ' . $baht . ' Baht',
        'description'   => 'Ledger Amount',
        'progress'      => 100, // integer
        'progressClass' => 'progress-bar bg-primary',
      ],
      [
        'type'          => 'progress_white',
        'class'         => 'card mb-2',
        'value'         => $wards . ' wards',
        'description'   => 'Registered Wards',
        'progress'      => 100, // integer
        'progressClass' => 'progress-bar bg-success',
      ],

      [
        'type'          => 'progress_white',
        'class'         => 'card mb-2',
        'value'         => $customers . ' customers',
        'description'   => 'Registered Customers',
        'progress'      => 100, // integer
        'progressClass' => 'progress-bar bg-warning',
      ],

      [
        'type'          => 'progress_white',
        'class'         => 'card mb-2',
        'value'         => $currency . ' currencies',
        'description'   => 'Registered Currencies',
        'progress'      => 100, // integer
        'progressClass' => 'progress-bar bg-dark',
      ],
    ]


  ];
  
$widgets['after_content'][] = [
  'type' => 'div',
  'class' => 'container-fluid row',
  'content' => [ // widgets 
    [
      'type' => 'chart',
      'wrapperClass' => 'col-md-6',
      // 'class' => 'col-md-6',
      'controller' => \App\Http\Controllers\Admin\Charts\NewEntriesForLedgerChartController::class,
      'content' => [
        'header' => 'New Entries For Ledgers (Last 7 Days)', // optional
        // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional

      ]
    ],
    [
      'type' => 'chart',
      'wrapperClass' => 'col-md-6',
      // 'class' => 'col-md-6',
      'controller' => \App\Http\Controllers\Admin\Charts\NewEntriesChartController::class,
      'content' => [
        'header' => 'New Entries for 7 Days', // optional
        // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
      ]
    ],
  ]
];
$widgets['after_content'][] = [
  'type' => 'div',
  'class' => 'container-fluid row',
  'content' => [ // widgets 
    [
      'type' => 'chart',
      'wrapperClass' => 'col-md-6',
      // 'class' => 'col-md-6',
      'controller' => \App\Http\Controllers\Admin\Charts\PrintedLedgerChartController::class,
      'content' => [
        'header' => 'Printed and Non-Printed Vouchers', // optional
        // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional

      ]
    ],
    // [
    //   'type' => 'chart',
    //   'wrapperClass' => 'col-md-6',
    //   // 'class' => 'col-md-6',
    //   'controller' => \App\Http\Controllers\Admin\Charts\CustomerByWardChartController::class,
    //   'content' => [
    //     'header' => 'Last 5 Customers By Wards', // optional
    //     // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
    //   ]
    // ],
  ]
];

?>