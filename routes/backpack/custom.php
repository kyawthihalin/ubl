<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('ward', 'WardCrudController');
    Route::crud('currency', 'CurrencyCrudController');
    Route::crud('customer', 'CustomerCrudController');
    Route::crud('ledger', 'LedgerCrudController');
    Route::post('getCustomer', 'LedgerCrudController@getCustomers')->name('getcustomers');

    Route::post('getWardReport', 'WardReportTransactionCrudController@getWardReport')->name('getWardReport');
    Route::post('getLedgerReport', 'ReportTransactionCrudController@getLedgerReport')->name('getLedgerReport');
    Route::post('getCustomerReport', 'CustomerReportTransactionCrudController@getCustomerReport')->name('getCustomerReport');
    Route::get('dashboard','DashboardController@index');

    Route::crud('reporttransaction', 'ReportTransactionCrudController');
    Route::crud('wardreporttransaction', 'WardReportTransactionCrudController');
    Route::crud('customerreporttransaction', 'CustomerReportTransactionCrudController');
    Route::get('charts/printed-ledger', 'Charts\PrintedLedgerChartController@response')->name('charts.printed-ledger.index');
    Route::get('charts/customer-by-ward', 'Charts\CustomerByWardChartController@response')->name('charts.customer-by-ward.index');
    Route::get('charts/new-entries', 'Charts\NewEntriesChartController@response')->name('charts.new-entries.index');
    Route::get('charts/new-entries-for-ledger', 'Charts\NewEntriesForLedgerChartController@response')->name('charts.new-entries-for-ledger.index');
}); // this should be the absolute last line of this file