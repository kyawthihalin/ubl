<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'ubl', 'namespace' => 'MobileApi','middleware'=>'auth:api'], function () {

    Route::get('wards','ApiController@getWards');

    Route::get('customers','ApiController@getCustomers');

    Route::post('get/customer','ApiController@searchCustomer');

    Route::get('currency','ApiController@getCurrency');

    Route::post('ledger/entry','ApiController@storeLedger');

    Route::post('get/customerByWard','ApiController@CustomerByWard');

});
Route::post('ubl/getToken','MobileApi\AuthenticateController@login');

