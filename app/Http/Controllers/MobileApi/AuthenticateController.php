<?php

namespace App\Http\Controllers\MobileApi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthenticateController extends Controller
{
   public function login(Request $request){

        $validate = Validator::make($request->all(),[
            'email'    => 'required',
            'password' => 'required',
        ]);

        if($validate->fails())
        {   
            return response()->json([
                'status'  => 500,
                'message' => 'Login Failed',
                'data'    => $validate->errors()
            ]);
        }
        
        $email       = $request->email;
        $password    = $request->password;
        
        $crenditials = ['email' => $email , 'password' => $password];
        if(Auth::attempt($crenditials)){
            $user    = Auth::user();
            $token   = $user->createToken('ubl')->accessToken;
            return response()->json([
                'status' => 200,
                'message'=> 'login success',
                'data'   => $user,
                'token'  => $token
            ]);
        }
        return response()->json([
            'status' => 500,
            'message' => 'Login Failed',
            'data'    =>  [
                'error' => 'Email or Password does not match'
            ]
        ]);

   }

}
