<?php

namespace App\Http\Controllers\MobileApi;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\Customer;
use App\Models\Ledger;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\GenerateID;
use Carbon\Carbon;

use function GuzzleHttp\json_decode;

class ApiController extends Controller
{
    public function getWards(){
        
        $wards = Ward::all();
        
        return response()->json([
            'status' => 200,
            'message' => 'Success',
            'data'    =>  [
                'wards' => $wards
            ]
        ]);
    }

    public function getCustomers(){

        $customers = Customer::all();

        return response()->json([
            'status' => 200,
            'message' => 'Success',
            'data'    =>  [
                'customers' => $customers
            ]
        ]);
    }

    public function CustomerByWard(Request $request){
        $validate = Validator::make($request->all(),[
            'ward_id'        => 'required',
        ]);
        
        if($validate->fails())
        {   
            return response()->json([
                'status'  => 500,
                'message' => 'Get Data Failed',
                'data'    => $validate->errors()
            ]);
        }

        $customers = Customer::where('ward_id',$request->ward_id)->with('ward','currency')->get();
        return response()->json([
            'status' => 200,
            'message' => 'Success',
            'data'    =>  [
                'customerByWard' => $customers
            ]
        ]);
    }
    public function getCurrency(){

        $currency = Currency::all();

        
        return response()->json([
            'status' => 200,
            'message' => 'Success',
            'data'    =>  [
                'currency' => $currency
            ]
        ]);
    }

    public function searchCustomer(Request $request){

        $startDate = Carbon::now();
        $firstDay  = $startDate->firstOfMonth();  
        $endDate   = Carbon::now();
        $endDate   = date_format($endDate,'Y-m-d');
        $firstDate = date_format($firstDay,'Y-m-d');

        $validate = Validator::make($request->all(),[
            'customer_id'    => 'required',
            'ward_id'        => 'required',
            'home_no'        => 'required'
        ]);
        
        if($validate->fails())
        {   
            return response()->json([
                'status'  => 500,
                'message' => 'Get Data Failed',
                'data'    => $validate->errors()
            ]);
        }

        $customer_id = $request->customer_id;
        $ward_id     = $request->ward_id;
        $home_no     = $request->home_no;
        $customer    = Customer::where('id',$customer_id)->where('ward_id',$ward_id)->where('home_no',$home_no)->first();
        if($customer){
            $check  = Ledger::where('customer_id',$customer->id)->where('ledger_date','>=',$firstDate)->where('ledger_date','<=',$endDate)->orderBy('id','desc')->first();
            if($check){
                return response()->json([
                    'status' => 200,
                    'message' => 'Success',
                    'data'    =>  [
                        'message' => 'This Customer had already Paid'
                    ]
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 200,
                    'message' => 'Success',
                    'data'    =>  [
                        'customer' => $customer
                    ]
                ]);
            }
        }
        return response()->json([
            'status' => 500,
            'message' => 'No Customer with this ID',
            'data'    =>  [
                'error' => 'No Data Found'
            ]
        ]);
    }   
    
    public function storeLedger(Request $request){
       
        $getLedgerDatas = json_decode($request->ledger);
        foreach($getLedgerDatas as $LedgerData){
            $customer_id   = $LedgerData->customer_id ?? null;
            $currency_id   = $LedgerData->currency_id ?? null;
            $amount        = $LedgerData->amount ?? null;
            $user_id       = $LedgerData->user_id ?? null;
            $reason        = $LedgerData->reason ?? null;
            $date          = $LedgerData->date ?? null;
            $last_seq      = Ledger::max('seq');
            $last_seq      = $last_seq > 0 ? $last_seq + 1 : 1;
            $ledger_id     = GenerateID::generateID('LG-00',$last_seq);
            $print_status  = 1;
            $discount      = 0;
            if($LedgerData->discount){
                $discount  = $LedgerData->discount;
            }
            $total         = $amount - $discount;
    
            Ledger::create([
                'customer_id' => $customer_id,
                'currency_id' => $currency_id,
                'amount'      => $amount,
                'user_id'     => $user_id,
                'ledger_date' => $date,
                'reason'      => $reason,
                'print_status'=> $print_status,
                'ledger_id'   => $ledger_id,
                'seq'         => $last_seq,
                'pay_back'    => $discount,
                'total'       => $total
            ]);
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Success',
        ]);
    }
}
