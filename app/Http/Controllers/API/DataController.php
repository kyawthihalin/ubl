<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Ward;
use App\Models\Currency;
class DataController extends Controller
{
    public function getallCustomers(Request $request){
        
        $search_term = $request->input('q');
        $page = $request->input('page');
        $job_id = $request->input('job_id');
        if ($search_term)
        {
        $results = Customer::where('customer_name', 'LIKE', '%'.$search_term.'%')

            ->paginate(100);
        }
        else
        {
            $results = Customer::paginate(100);
        }

        return $results;
    }

    public function getallWards(Request $request){
        
        $search_term = $request->input('q');
        $page = $request->input('page');
        $job_id = $request->input('job_id');
        if ($search_term)
        {
        $results = Ward::where('customer_name', 'LIKE', '%'.$search_term.'%')

            ->paginate(100);
        }
        else
        {
            $results = Ward::paginate(100);
        }

        return $results;
    }
    public function getallCurrency(Request $request){
        
        $search_term = $request->input('q');
        $page = $request->input('page');
        $job_id = $request->input('job_id');
        if ($search_term)
        {
        $results = Currency::where('customer_name', 'LIKE', '%'.$search_term.'%')

            ->paginate(100);
        }
        else
        {
            $results = Currency::paginate(100);
        }

        return $results;
    }
    
}
