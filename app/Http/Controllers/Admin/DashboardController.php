<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Currency;
use App\Models\Ledger;
use App\Models\Ward;
use App\User;
class DashboardController extends Controller
{
    public function index()
    {
        if(backpack_user()->can('dashboard')){
            $today = date("Y-m-d");
            $mmk = Ledger::where('currency_id',2)->whereDate('ledger_date','=',$today)->get()->sum('amount');
            $baht = Ledger::where('currency_id',1)->whereDate('ledger_date','=',$today)->get()->sum('amount');
            $customers = Customer::all()->count();
            $wards = Ward::all()->count();
            $currency = Currency::all()->count();
            return view(backpack_view('dashboard'),compact('customers','wards','currency','mmk','baht'));
        }else{
            return view('errors.401');
        }
       
    }
}
