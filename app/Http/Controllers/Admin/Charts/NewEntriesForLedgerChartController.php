<?php

namespace App\Http\Controllers\Admin\Charts;

use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\Models\Ledger;
/**
 * Class NewEntriesForLedgerChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NewEntriesForLedgerChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels(['6 days ago', '5 days ago', '4 days ago', '3 days ago', '2 days ago', 'Yesterday', 'Today']);


        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/new-entries-for-ledger'));

        // OPTIONAL
        // $this->chart->minimalist(false);
        // $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $today_ledgers = Ledger::whereDate('ledger_date', today())->count();
        $yesterday_ledgers = Ledger::whereDate('ledger_date', today()->subDays(1))->count();
        $ledgers_2_days_ago = Ledger::whereDate('ledger_date', today()->subDays(2))->count();
        $ledgers_3_days_ago = Ledger::whereDate('ledger_date', today()->subDays(3))->count();
        $ledgers_4_days_ago = Ledger::whereDate('ledger_date', today()->subDays(4))->count();
        $ledgers_5_days_ago = Ledger::whereDate('ledger_date', today()->subDays(5))->count();
        $ledgers_6_days_ago = Ledger::whereDate('ledger_date', today()->subDays(6))->count();

        $this->chart->dataset('New Entries For Ledgers (Last 7 Days)', 'bar', [
            $ledgers_6_days_ago,
            $ledgers_5_days_ago,
            $ledgers_4_days_ago,
            $ledgers_3_days_ago,
            $ledgers_2_days_ago,
            $yesterday_ledgers,
            $today_ledgers,
        ])->color('rgb(66, 186, 150, 1)')
            ->backgroundColor('rgb(66, 186, 150, 0.4)');
    }
}