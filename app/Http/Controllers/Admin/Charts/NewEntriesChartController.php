<?php

namespace App\Http\Controllers\Admin\Charts;

use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\Models\Ward;
use App\Models\Customer;
use App\Models\Ledger;
use Illuminate\Support\Facades\DB;
/**
 * Class NewEntriesChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NewEntriesChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();
       
          // MANDATORY. Set the labels for the dataset points
          $labels = [];
          for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {
              if ($days_backwards == 1) {
              }
              $labels[] = $days_backwards.' days ago';
          }
          $this->chart->labels($labels);
          // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/new-entries'));

    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {
            // Could also be an array_push if using an array rather than a collection.
            $wards[] = Ward::whereDate('created_at', today()->subDays($days_backwards))
                            ->count();
            $customers[] = Customer::whereDate('created_at', today()->subDays($days_backwards))
                            ->count();
            $ledgers[] = Ledger::whereDate('ledger_date', today()->subDays($days_backwards))
                            ->count();
         
        }

        $this->chart->dataset('Wards', 'line', $wards)
            ->color('rgb(66, 186, 150)')
            ->backgroundColor('rgba(66, 186, 150, 0.4)');

        $this->chart->dataset('Customers', 'line', $customers)
            ->color('rgb(96, 92, 168)')
            ->backgroundColor('rgba(96, 92, 168, 0.4)');

        $this->chart->dataset('Ledgers', 'line', $ledgers)
            ->color('rgb(255, 193, 7)')
            ->backgroundColor('rgba(255, 193, 7, 0.4)');

    }
}