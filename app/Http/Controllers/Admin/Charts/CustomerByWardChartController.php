<?php

namespace App\Http\Controllers\Admin\Charts;

use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\Models\Ward;
use Illuminate\Support\Facades\DB;
/**
 * Class CustomerByWardChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CustomerByWardChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();
        $customerByWard = DB::table('customers')->select('ward_id',DB::raw('count(*) as total'))->groupBy('ward_id')->orderBy('id','desc')->take(5)->get();
        $total = [];
        $ward = [];
        $wardName = [];
        foreach($customerByWard as $customer){
            $total[]= $customer->total;
            $ward[]= $customer->ward_id;
        }
        $wards = Ward::whereIn('id',$ward)->orderBy('id','desc')->get();
        foreach($wards as $ward){
            $wardName[]=$ward->ward_name;
        }
        $this->chart->dataset('Red', 'pie', $total)
                    ->backgroundColor([
                        'rgb(130, 22, 94)',
                        'rgb(179, 88, 36)',
                        'rgb(66, 186, 150)',
                        'rgb(96, 92, 168)',
                        'rgb(255, 193, 7)',
                    ]);

        // OPTIONAL
        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($wardName);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/customer-by-ward'));

        // OPTIONAL
        // $this->chart->minimalist(false);
        // $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}