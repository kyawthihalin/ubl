<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Customer;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\Models\Ledger;
use Carbon\Carbon;

/**
 * Class PrintedLedgerChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PrintedLedgerChartController extends ChartController
{
    public function setup()
    {
       

        $this->chart = new Chart();
        $mytime = Carbon::now();
        $days = $mytime->daysInMonth;
        $currentYear = $mytime->format("Y-m");
        $startDate = $currentYear .'-01';
        $endDate = $currentYear .'-'.$days;
        $customers = Customer::all();
        $count = Customer::all()->count();
        foreach($customers as $customer){
            $cus_array[] = $customer->id;
        }
        if($cus_array!=null || $cus_array!=''){
            $printed = Ledger::whereIn('customer_id',$cus_array)->whereDate('ledger_date','>=', $startDate)->whereDate('ledger_date','<=', $endDate)->count();
            $nonprinted = $count - $printed;
            $arr = [$printed , $nonprinted];
            $this->chart->dataset('Red', 'pie', $arr)
                        ->backgroundColor([
                            'rgb(70, 127, 208)',
                            'rgb(66, 186, 150)',
                        ]);
    
            // OPTIONAL
            $this->chart->displayAxes(false);
            $this->chart->displayLegend(true);
    
            // MANDATORY. Set the labels for the dataset points
            $this->chart->labels(['Printed', 'Non-Printed']);
            // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
            $this->chart->load(backpack_url('charts/printed-ledger'));
        }
       

    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}
