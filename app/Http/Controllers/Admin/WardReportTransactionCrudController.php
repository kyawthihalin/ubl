<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\WardReportTransactionRequest;
use App\Models\Ward;
use App\Models\WardReportTransaction;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class WardReportTransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class WardReportTransactionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\WardReportTransaction::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/wardreporttransaction');
        CRUD::setEntityNameStrings('wardreporttransaction', 'Ward Report');
        if(backpack_user()->can('ward-report')){
            CRUD::denyAccess(['list']);
        }else{
            CRUD::denyAccess(['list','create']);
        }
    }


    protected function setupCreateOperation()
    {
        CRUD::setValidation(WardReportTransactionRequest::class);
       
        $this->crud->addField([
            'name' => 'start_date', // a unique name for this field
            'label' => 'Select Start Date',
            'type' => 'date_picker',
            'default' => Carbon::today()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]
          
        ]);
        $this->crud->addField([
            'name' => 'end_date', // a unique name for this field
            'label' => 'Select End Date',
            'type' => 'date_picker',
            'default' => Carbon::tomorrow()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ]
        ]);
     
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/main-script'
        ]);
    }
    
    public function getWardReport(Request $request){
        $start_date= $request->start_date;
        $end_date= $request->end_date;
        $wards = WardReportTransaction::whereDate('created_at', '>=', $start_date)->whereDate('created_at', '<=', $end_date)->get();
        return view('partials.report',compact('wards','start_date','end_date'));
    }
  
}
