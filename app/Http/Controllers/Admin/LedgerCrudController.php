<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\GenerateID;
use App\Http\Requests\LedgerRequest;
use App\Models\Customer;
use App\Models\Ledger;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Prologue\Alerts\Facades\Alert;
use Carbon\Carbon;
/**
 * Class LedgerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LedgerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Ledger::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ledger');
        CRUD::setEntityNameStrings('ledger', 'ledgers');
        CRUD::denyAccess('show');
        CRUD::enableExportButtons();
        GenerateID::setPermissions('ledger', $this->crud);
        
        $this->crud->addFilter([ // simple filter
            'type' => 'text',
            'name' => 'ledger_id',
            'label' => "Ledger ID"
        ],
        false,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', function ($q) use ($value) {
                return $q->orWhere('ledger_id', 'LIKE', "%{$value}%");
            });
        });

          
        $this->crud->addFilter([ // simple filter
            'type' => 'text',
            'name' => 'customer_id',
            'label' => "Customer ID"
        ],
        false,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', function ($q) use ($value) {
            $ids=[];
            $customers = Customer::where('customer_id','like','%'.$value.'%')->get();
            foreach($customers as $customer){
                $ids[]=json_decode($customer->id);
            }
            $this->crud->addClause('whereIn', 'customer_id',$ids);
            });
        });


        $this->crud->addFilter([ // daterange filter
            'type' => 'date_range',
            'name' => 'from_to',
            'label' => 'Date range'
        ],
        false,
        function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('where', 'ledger_date', '>=', $dates->from);
            $this->crud->addClause('where', 'ledger_date', '<=', $dates->to . ' 23:59:59');
        });

        $this->crud->addFilter([
            'name'  => 'print_status',
            'type'  => 'select2',
            'label' => 'Status'
          ], [
              '1' => 'Printed Vouchers',
              '0' => 'Non-Printed Vouchers',
          ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'print_status', $value);
        });
        $this->crud->addFilter([
            'name'  => 'currency_id',
            'type'  => 'select2',
            'label' => 'Currency'
          ], [
              '2' => 'MMK',
              '1' => 'Baht',
          ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'currency_id', $value);
        });
        
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn(['label'=>'Ledger ID','name' => 'ledger_id', 'type' => 'text']); 
        $this->crud->addColumn([
            'label' => 'Customer ID', // Table column heading
            'type' => "select",
            'name' => 'customer_id', 
            'entity' => 'customer', // the method that defines the relationship in your Model
            'attribute' => "customer_id", // foreign key attribute that is shown to user
            'model' => "App\Models\Customer", // foreign key model
        ]);
        $this->crud->addColumn([
            'label' => 'Customer Name', // Table column heading
            'type' => "select",
            'name' => 'customer', 
            'entity' => 'customer', // the method that defines the relationship in your Model
            'attribute' => "customer_name", // foreign key attribute that is shown to user
            'model' => "App\Models\Customer", // foreign key model
        ]);
       
        
        CRUD::addColumn([
            'name' => 'print_status', // The db column name
            'label' => "Print Status", // Table column heading
            'type' => 'check',
        ]); 
        CRUD::addColumn([
            'name' => 'ledger_date', // The db column name
            'label' => "Date", // Table column heading
            'type' => 'date',
        ]); 
        $this->crud->addColumn([
            'label' => 'Currency Name', // Table column heading
            'type' => "select",
            'name' => 'currency', 
            'entity' => 'currency', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Currency", // foreign key model
        ]);
        CRUD::addColumn(['label'=>'Amount','name' => 'amount', 'type' => 'text']); 
        CRUD::addColumn([
            'name' => 'pay_back', // The db column name
            'label' => "PayBack", // Table column heading
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'reason', // The db column name
            'label' => "Reason", // Table column heading
            'type' => 'text',
        ]); 
        CRUD::addColumn([
            'name' => 'total', // The db column name
            'label' => "Total Amount", // Table column heading
            'type' => 'text',
        ]); 
        $this->crud->addColumn([
            'label' => 'Printed Team Name', // Table column heading
            'type' => "select",
            'name' => 'user', 
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\User", // foreign key model
        ]);

    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LedgerRequest::class);
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/ward'
        ]);
        $this->crud->addField([
            'name' => 'ledger_id',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'seq',
            'type' => 'hidden',
        ]);
       
        $this->crud->addField([
            'name' => 'amount',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'name' => 'pay_back',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'currency', // the relationship name in your Model
            'entity' => 'currency', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'name' => 'ledger_date',
            'type' => 'date',
            'label' => 'Ledger Date',
            'default' => Carbon::today()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'name' => 'reason',
            'type' => 'textarea',
            'label' => 'Remark',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'label'=> 'Print User',
            'name' => 'user', // the relationship name in your Model
            'entity' => 'user', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'name' => 'print_status',
            'label' => 'Print status',
            'type' => 'checkbox',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        
        ]);
      
      
    }
    public function store(LedgerRequest $request){
        $validate = $request->validate([
            'ward' => 'required',
            'customer_id' => 'required',
            'currency' => 'required',
            'ledger_date' => 'required',
            'amount' => 'required',
            'reason' => 'required',
        ]);
        if($request->pay_back!=null || $request->pay_back!=0){
            $payback = $request->pay_back;
            $total = $request->amount - $payback;
        }else{
            $payback = 0;
            $total = $request->amount - $payback;
        }
        
        Ledger::create([
            'customer_id'  => $request->customer_id,
            'user_id'  => $request->user,
            'amount' => $request->amount,
            'currency_id' => $request->currency,
            'ledger_date' => $request->ledger_date,
            'reason' => $request->reason,
            'print_status' => $request->print_status,
            'pay_back'=> $payback,
            'total' => $total

        ]);
        \Alert::add('success', 'Item Successfully Saved')->flash();
        return redirect('admin/ledger');
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function update(LedgerRequest $request){
        $validate = $request->validate([
            'ward' => 'required',
            'customer_id' => 'required',
            'currency' => 'required',
            'ledger_date' => 'required',
            'amount' => 'required',
            'reason' => 'required',
        ]);
        if($request->pay_back!=null || $request->pay_back!=0){
            $payback = $request->pay_back;
            $total = $request->amount - $payback;
        }else{
            $payback = 0;
            $total = $request->amount - $payback;
        }
        $data= [
            'customer_id'  => $request->customer_id,
            'user_id'  => $request->user,
            'amount' => $request->amount,
            'currency_id' => $request->currency,
            'ledger_date' => $request->ledger_date,
            'reason' => $request->reason,
            'print_status' => $request->print_status,
            'pay_back'=> $payback,
            'total' => $total
        ];
        Ledger::whereId($request->id)->update($data);
        \Alert::add('success', 'Item Successfully Modified')->flash();
        return redirect('admin/ledger');
    }
    public function getCustomers(Request $request){
        $customers = Customer::where('ward_id','=',$request->ward_id)->get();
        return view('partials/customer',compact('customers'));
    }
}
