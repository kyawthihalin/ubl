<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReportTransactionRequest;
use App\Models\ReportTransaction;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportTransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReportTransactionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ReportTransaction::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/reporttransaction');
        CRUD::setEntityNameStrings('reporttransaction', 'Ledger Report');
        if (backpack_user()->can('ledger-report')) {
            CRUD::denyAccess(['list']);
        } else {
            CRUD::denyAccess(['list', 'create']);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReportTransactionRequest::class);
        $this->crud->addField([
            'name' => 'start_date', // a unique name for this field
            'label' => 'Select Start Date(Only For Printed Vouchers)',
            'type' => 'date_picker',
            'default' => Carbon::today()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]

        ]);
        $this->crud->addField([
            'name' => 'end_date', // a unique name for this field
            'label' => 'Select End Date(Only For Printed Vouchers)',
            'type' => 'date_picker',
            'default' => Carbon::tomorrow()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'name'  => 'month',
            'label' => 'Select Month(Only For Non-printed Vouchers)',
            'type'  => 'month',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'type' => 'select2_from_ajax_multiple',
            'name' => 'customer', // the relationship name in your Model
            'label' => 'Select Customers(Only For printed Vouchers)',
            'entity' => 'customer', // the relationship name in your Model
            'attribute' => 'customer_name', // attribute on Article that is shown to admin
            'placeholder' => "Select a Customer", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'data_source' => url("api/allcustomer"), // url to controller search function (with /{id} should return model)
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select2_from_ajax_multiple',
            'name' => 'ward', // the relationship name in your Model
            'entity' => 'ward', // the relationship name in your Model
            'attribute' => 'ward_name', // attribute on Article that is shown to admin
            'placeholder' => "Select a Ward", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'data_source' => url("api/allward"), // url to controller search function (with /{id} should return model)
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select2_from_ajax',
            'name' => 'currency', // the relationship name in your Model
            'entity' => 'currency', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin
            'placeholder' => "Select a Currency", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'data_source' => url("api/allcurrency"), // url to controller search function (with /{id} should return model)
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField(
            [   // select2_from_array
                'name' => 'print_status',
                'label' => "Print Status",
                'type' => 'select2_from_array',
                'options' => ['0' => 'Only Non-Printed Voucher', '1' => 'Only Printed Voucher'],
                'allows_null' => true,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ],
            ]
        );
       
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/ledger-script'
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getLedgerReport(Request $request)
    {
        if ($request->print_status == 1 || $request->print_status == null) {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $ward[] = $request->ward_id;
            $customers[] = $request->customer_id;
            $curreny = $request->currency_id;
            $print_status = $request->print_status;
            $ledgers = ReportTransaction::where(function ($query) use ($start_date, $end_date) {
                return $query->whereDate('ledger_date', '>=', $start_date)
                    ->whereDate('ledger_date', '<=', $end_date);
                })->where(function ($query) use ($ward) {
                    if ($ward[0] != null) {
                        $customer_ward = DB::table('customers')->whereIn('ward_id', $ward[0])->get();
                        foreach ($customer_ward as $cus) {
                            $ids[] = $cus->id;
                        }
                        return $query->whereIn('customer_id', $ids);
                    }
                })
                ->where(function ($query) use ($customers) {
                    if ($customers[0] != null) {
                        return $query->whereIn('customer_id', $customers[0]);
                    }
                })
                ->where(function ($query) use ($curreny) {
                    if ($curreny != null) {
                        return $query->where('currency_id', $curreny);
                    }
                })->where(function ($query) use ($print_status) {
                    if ($print_status != null) {
                        return $query->where('print_status', $print_status);
                    }
                })->get();
            $ledgerTotalBaht = DB::table("ledgers")->where('currency_id', 1)->whereDate('ledger_date', '>=', $start_date)->whereDate('ledger_date', '<=', $end_date)->get()->sum("total");
            $ledgerTotal = DB::table("ledgers")->where('currency_id', 2)->whereDate('ledger_date', '>=', $start_date)->whereDate('ledger_date', '<=', $end_date)->get()->sum("total");
            return view('partials.ledgerReport', compact('ledgers', 'start_date', 'end_date', 'ledgerTotalBaht', 'ledgerTotal'));
        }else{
            $mytime = $request->month;
            $date = explode('-', $mytime);
            $days = cal_days_in_month(CAL_GREGORIAN, $date[1], $date[0]);
            $startDate = $mytime . '-01';
            $endDate = $mytime . '-' . $days;
            $ward[] = $request->ward_id;
            $curreny = $request->currency_id;
            $led_array=[];
            $non_printed_customers = Customer::where(function ($query) use ($startDate, $endDate) {
                $printed_ledgers = ReportTransaction::whereDate('ledger_date', '>=', $startDate)->whereDate('ledger_date', '<=', $endDate)->get();
                if(count($printed_ledgers)>0){
                    foreach ($printed_ledgers as $ledger) {
                        $led_array[] = $ledger->customer_id;
                    };
                    return $query->whereNotIn('id', $led_array)->get();
                }else{

                }
            })->where(function ($query) use ($ward) {
                if ($ward[0] != null) {
                    $customer_ward = Customer::whereIn('ward_id', $ward[0])->get();
                    foreach ($customer_ward as $cus) {
                        $ids[] = $cus->id;
                    }
                    return $query->whereIn('id', $ids);
                }
            })->where(function ($query) use ($curreny) {
                if ($curreny != null) {
                    return $query->where('currency_id', $curreny);
                }
            })->get();

            $non_printed_customers = $non_printed_customers->filter(function ($item) use ($startDate, $endDate) {
                return (data_get($item, 'created_at') >= $startDate) && (data_get($item, 'created_at') <= $endDate);
            });

            // $non_printed_customers = $non_printed_customers->filter(function ($item) use ($startDate, $endDate) {
            //     return (data_get($item, 'created_at') <= $startDate);
            // });

            $ids = [];
            $currenyids = [];
            if(count($non_printed_customers)>0){
                foreach($non_printed_customers as $voucher){
                    $ids[] = $voucher->id;
                }
                foreach($non_printed_customers as $currnecy){
                    $currenyids[] = $currnecy->currency_id;
                }
                $totalMMK=0;
                $totalBaht = 0;
                if(in_array(2,$currenyids)){
                    $totalMMK = Customer::whereIn('id', $ids)->where('currency_id', 2)->get()->sum("amount");
                }
                if(in_array(1,$currenyids)){
                    $totalBaht = Customer::whereIn('id', $ids)->where('currency_id', 1)->get()->sum("amount");
                }
            }
            return view('partials.nonledgerReport', compact('non_printed_customers', 'startDate', 'endDate', 'totalBaht', 'totalMMK'));
        }
    }
}
