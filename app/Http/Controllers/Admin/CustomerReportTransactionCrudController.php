<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CustomerReportTransactionRequest;
use App\Models\CustomerReportTransaction;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class CustomerReportTransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CustomerReportTransactionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\CustomerReportTransaction::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/customerreporttransaction');
        CRUD::setEntityNameStrings('customerreporttransaction', 'Customer Report');
        if(backpack_user()->can('customer-report')){
            CRUD::denyAccess(['list']);
        }else{
            CRUD::denyAccess(['list','create']);
        }
    }

   

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CustomerReportTransactionRequest::class);
        $this->crud->addField([
            'name' => 'start_date', // a unique name for this field
            'label' => 'Select Start Date',
            'type' => 'date_picker',
            'default' => Carbon::today()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'name' => 'end_date', // a unique name for this field
            'label' => 'Select End Date',
            'type' => 'date_picker',
            'default' => Carbon::tomorrow()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ]
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'ward', // the relationship name in your Model
            'entity' => 'ward', // the relationship name in your Model
            'attribute' => 'ward_name', // attribute on Article that is shown to admin
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/customer-script'
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getCustomerReport(Request $request){
        $start_date= $request->start_date;
        $end_date= $request->end_date;
        $ward = $request->ward;
        if($request->ward){
            $customers = CustomerReportTransaction::where(function($query) use ($start_date,$end_date){
                return $query->whereDate('created_at', '>=', $start_date)
                        ->whereDate('created_at', '<=', $end_date);
            })->where(function ($query) use ($ward){
                return $query->where('ward_id', $ward);
            })->get();
        }else{
            $customers = CustomerReportTransaction::where(function($query) use ($start_date,$end_date){
                return $query->whereDate('created_at', '>=', $start_date)
                        ->whereDate('created_at', '<=', $end_date);
            })->get();
        }
        
        return view('partials.customerReport',compact('customers','start_date','end_date'));
    }
}
