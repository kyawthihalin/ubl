<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\GenerateID;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use App\Models\Ward;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\App;
use Prologue\Alerts\Facades\Alert;
use function PHPSTORM_META\type;

/**
 * Class CustomerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CustomerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Customer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/customer');
        CRUD::setEntityNameStrings('customer', 'customers');
        CRUD::denyAccess('show');
        CRUD::enableExportButtons();
        GenerateID::setPermissions('customer', $this->crud);

        $this->crud->addFilter([ // simple filter
            'type' => 'text',
            'name' => 'customer_id',
            'label' => "Customer ID"
        ],
        false,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', function ($q) use ($value) {
                return $q->orWhere('customer_id', 'LIKE', "%{$value}%");
            });
        });
        
        $this->crud->addFilter([ // simple filter
            'type' => 'text',
            'name' => 'ward_id',
            'label' => "Ward Name"
        ],
        false,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', function ($q) use ($value) {
            $ids=[];
            $wards = Ward::where('ward_name','like','%'.$value.'%')->get();
            foreach($wards as $ward){
                $ids[]=json_decode($ward->id);
            }
            $this->crud->addClause('whereIn', 'ward_id',$ids);
            });
        });

        $this->crud->addFilter([ // simple filter
            'type' => 'text',
            'name' => 'home_no',
            'label' => "Home Number"
        ],
        false,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', function ($q) use ($value) {
                return $q->orWhere('home_no', 'LIKE', "%{$value}%");
            });
        });
        $this->crud->addFilter([ // daterange filter
            'type' => 'date_range',
            'name' => 'from_to',
            'label' => 'Date range'
        ],
        false,
        function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('where', 'created_at', '>=', $dates->from);
            $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
        });
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn(['label'=>'Customer ID','name' => 'customer_id', 'type' => 'text']); 
        CRUD::addColumn(['name' => 'customer_name', 'type' => 'text']); 
        CRUD::addColumn(['name' => 'home_no', 'type' => 'text']); 
        $this->crud->addColumn([
            'label' => 'Ward', // Table column heading
            'type' => "select",
            'name' => 'ward_id', 
            'entity' => 'ward', // the method that defines the relationship in your Model
            'attribute' => "ward_name", // foreign key attribute that is shown to user
            'model' => "App\Models\Ward", // foreign key model
        ]);
        CRUD::addColumn(['name' => 'amount', 'type' => 'text']); 
        $this->crud->addColumn([
            'label' => 'Currency', // Table column heading
            'type' => "select",
            'name' => 'currency', 
            'entity' => 'currency', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Currency", // foreign key model
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CustomerRequest::class);
        $this->crud->addField([
            'name' => 'customer_id',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'seq',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'label' => "Customer Name", // Table column heading
            'name' => 'customer_name', // the column that contains the ID of that connected entity
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'ward', // the relationship name in your Model
            'entity' => 'ward', // the relationship name in your Model
            'attribute' => 'ward_name', // attribute on Article that is shown to admin
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'label' => "Home Number", // Table column heading
            'name' => 'home_no', // the column that contains the ID of that connected entity
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
         
        ]);
        $this->crud->addField([
            'label' => 'Amount',
            'name' => 'amount',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'currency', // the relationship name in your Model
            'entity' => 'currency', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    public function store(CustomerRequest $request)
    {   
        Customer::create([
            'ward_id'  => $request->ward,
            'customer_name' => $request->customer_name,
            'home_no' => $request->home_no,
            'amount' => $request->amount,
            'currency_id' => $request->currency
        ]);
        \Alert::add('success', 'Item Successfully Saved')->flash();
        return redirect('admin/customer');
    }
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
