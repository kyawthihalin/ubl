<?php
/**
 * Created by VSCODE.
 * User: KyawThiHaLin
 * Date: 2020-09-20
 * Time: 14:17
 */
namespace App\Helpers;

class GenerateID{
    public static function generateID($prefix,$lastID){
        $customID = $prefix.$lastID;
        return $customID;
    }
    public static function setPermissions($permission, $crud){
        $crud->denyAccess(['list', 'create', 'update', 'delete', 'clone']);
        // Allow list access
        if (backpack_user()->can('list-'.$permission)) {
            $crud->allowAccess('list');
        }
        // Allow create access
        if (backpack_user()->can('create-'.$permission)) {
            $crud->allowAccess('create');
        }
        // Allow update access
        if (backpack_user()->can('update-'.$permission)) {
            $crud->allowAccess('update');
        }
        // Allow delete access
        if (backpack_user()->can('delete-'.$permission)) {
            $crud->allowAccess('delete');
        }
        // Allow clone access
        if (backpack_user()->can('clone-'.$permission)) {
            $crud->allowAccess('clone');
        }
    }

}
