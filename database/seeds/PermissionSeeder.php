<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'create-customer', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'update-customer', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'delete-customer', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'list-customer', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
     
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'list-currency', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
          
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'create-ward', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'update-ward', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'delete-ward', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'list-ward', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
          
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'create-ledger', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'update-ledger', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'delete-ledger', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'list-ledger', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'ledger-report', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'ward-report', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'customer-report', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'reporting', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        DB::table('permissions')->insert(['guard_name' => 'web', 'name' => 'dashboard', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        

    }
}
